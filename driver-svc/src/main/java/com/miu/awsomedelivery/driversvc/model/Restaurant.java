package com.miu.awsomedelivery.driversvc.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Restaurant {
    private String id;
    private String name;
    private String phoneNumber;
    private String email;
    private Address address;
}
