package com.miu.awsomedelivery.driversvc.dto;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

import com.miu.awsomedelivery.driversvc.Enum.OrderStatus;
import com.miu.awsomedelivery.driversvc.model.Customer;
import com.miu.awsomedelivery.driversvc.model.Driver;
import com.miu.awsomedelivery.driversvc.model.Menu;
import com.miu.awsomedelivery.driversvc.model.Order;
import com.miu.awsomedelivery.driversvc.model.Restaurant;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderResponse {
    private String id;
    private String orderDate;
    private Customer customer;
    private Restaurant restaurant;
    private Driver driver;
    private Collection<Menu> menus;
    private Double totalPrice;
    private OrderStatus status;

    public static OrderResponse create(Order order) {
        OrderResponse res = new OrderResponse();
        res.setId(order.getOrderId());
        res.setOrderDate(order.getOrderDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        res.setCustomer(order.getCustomer());
        res.setRestaurant(order.getRestaurant());
        res.setDriver(order.getDriver());
        res.setMenus(order.getMenus());
        res.setStatus(order.getStatus());
        res.setTotalPrice(order.getTotalPrice());
        return res;
    }
}
