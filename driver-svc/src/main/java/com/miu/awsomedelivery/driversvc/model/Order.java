package com.miu.awsomedelivery.driversvc.model;

import java.time.LocalDateTime;
import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.miu.awsomedelivery.driversvc.Enum.OrderStatus;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Order {
    @Id
    private String orderId;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime orderDate;
    private Customer customer;
    private Restaurant restaurant;
    private Driver driver;
    private Collection<Menu> menus;
    private Double totalPrice;
    private OrderStatus status;
}
