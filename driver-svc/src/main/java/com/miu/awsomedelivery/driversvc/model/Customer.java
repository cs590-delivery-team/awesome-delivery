package com.miu.awsomedelivery.driversvc.model;

import lombok.Data;

@Data
public class Customer {
     private String id;
     private String name;
     private String phoneNumber;
     private String email;
     private Address address;
}
