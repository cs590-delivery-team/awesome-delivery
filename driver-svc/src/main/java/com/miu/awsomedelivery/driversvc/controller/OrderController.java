package com.miu.awsomedelivery.driversvc.controller;

import com.miu.awsomedelivery.driversvc.dto.OrderResponse;
import com.miu.awsomedelivery.driversvc.model.Order;
import com.miu.awsomedelivery.driversvc.services.DriverService;
import com.miu.awsomedelivery.driversvc.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("drivers/{driverID}/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private DriverService driverService;
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    // Pick order
    @PostMapping("/{orderID}/pickOrder")
    public ResponseEntity<Boolean> pickOrder(@PathVariable String driverID, @PathVariable String orderID){
        try {
            return ResponseEntity.ok(this.driverService.pickOrder(driverID,orderID));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    // Deliver order
    @PostMapping("/{orderID}/deliverOrder")
    public ResponseEntity<Boolean> deliverOrder(@PathVariable String orderID ){
        try {
            return ResponseEntity.ok(this.driverService.deliverOrder(orderID));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping
    //get orders which are ready
    public ResponseEntity<Collection<OrderResponse>> getAllOrders(){
        Collection<OrderResponse> res = orderService.getAllOrders().stream().map(OrderResponse::create).collect(Collectors.toList());
        return ResponseEntity.ok(res);
    }
}
