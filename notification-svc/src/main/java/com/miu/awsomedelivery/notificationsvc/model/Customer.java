package com.miu.awsomedelivery.notificationsvc.model;

import lombok.Data;

@Data
public class Customer {
     private String id;
     private String name;
     private String phoneNumber;
     private String email;
     private Address address;
}
