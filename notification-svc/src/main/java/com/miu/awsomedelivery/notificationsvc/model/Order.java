package com.miu.awsomedelivery.notificationsvc.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.miu.awsomedelivery.notificationsvc.Enum.OrderStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Order {
    private String orderId;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime orderDate;
    private Customer customer;
    private Restaurant restaurant;
    private Driver driver;
    private Double totalPrice;
    private OrderStatus status;
}
