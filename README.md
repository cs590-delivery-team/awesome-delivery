# Awesome Delivery

Awesome delivery is a prepared food delivery service. It uses several microservice for doing its task!

## Requirements and Goals of the system

Actors of the system:
- Customers
- Restaurants
- Drivers

Functional requirements
- Customer should search for restaurants, choose menu items and create an order.
- Customer should be able to see the status of its order.
- Restaurants should be able to see orders and accept/reject them.
- Drivers should be able to see orders that are ready for pick up and deliver them.
- All actors should get a notification.

## System Design

![Architecture diagram](./arch.png "Architecture")

## Deployment

The service runs on the Kubernetes cluster. Therefore, we need to prepare the infrastructure before deployment.

Preparing infrastructure (Google Cloud):

1. Setup [Google Kubernetes Engine](https://istio.io/latest/docs/setup/platform-setup/gke/) & install [Istio](https://istio.io/latest/docs/setup/getting-started/)
2. Install Infra services using [helm](https://helm.sh)
```bash
$ ./deploy/scripts/setup_infra.sh
```
3. Deploy services
```bash
$ kubectl apply -f ./deploy
```

## Istio configrations

### Circut breakers

The circuit breaker is configured for each service. For instance, User service is scanned every 5 mins and if there are 7 consecutive times 5XX errors, the pod will be ejected for 15 minutes.

### Authorization

Service to service authorization has been configured. i.e. Only API Gateway has the authority to communicate to the reset of microservices.

### Gateway

Only the client and API gateway are exposed to an external (public) network.