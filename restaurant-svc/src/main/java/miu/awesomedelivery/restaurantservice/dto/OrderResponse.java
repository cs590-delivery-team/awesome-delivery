package miu.awesomedelivery.restaurantservice.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

import lombok.Data;
import lombok.NoArgsConstructor;
import miu.awesomedelivery.restaurantservice.emuns.OrderStatus;
import miu.awesomedelivery.restaurantservice.model.Order;

@Data
@NoArgsConstructor
public class OrderResponse {
    private String id;

    private String orderDate;
    private Customer customer;
    private String restaurantId;
    private Collection<OrderMenu> menus;
    private OrderStatus status;
    private Double totalPrice;

    public static OrderResponse create(Order order) {
        OrderResponse res = new OrderResponse();
        res.setId(order.getId());
        res.setOrderDate(order.getOrderDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        res.setCustomer(order.getCustomer());
        res.setRestaurantId(order.getRestaurantId());
        res.setMenus(order.getMenus());
        res.setStatus(order.getStatus());
        res.setTotalPrice(order.getPrice());
        return res;
    }
}
