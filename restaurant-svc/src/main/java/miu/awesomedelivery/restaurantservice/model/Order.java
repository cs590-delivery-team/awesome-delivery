package miu.awesomedelivery.restaurantservice.model;

import java.time.LocalDateTime;
import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import miu.awesomedelivery.restaurantservice.dto.Customer;
import miu.awesomedelivery.restaurantservice.dto.OrderMenu;
import miu.awesomedelivery.restaurantservice.emuns.OrderStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document
public class Order {

    @Id
    private String id;
    // @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime orderDate;
    private OrderStatus status;
    private Customer customer;
    private String restaurantId;
    private Collection<OrderMenu> menus;
    private double price;
}
