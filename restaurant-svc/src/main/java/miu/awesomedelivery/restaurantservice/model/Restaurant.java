package miu.awesomedelivery.restaurantservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document
public class Restaurant {

    @Id
    private String id;
    private String name;
    private String userType;
    private String phoneNumber;
    private String email;
    private Address address;
    private List<Menu> menus = new ArrayList<>();

}
